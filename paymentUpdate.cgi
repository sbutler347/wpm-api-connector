#!/m1/shared/bin/perl

use strict;
use warnings;
use POSIX 'strftime';
use CGI;
use CGI::Carp;
use Data::Dumper;
use XML::LibXML;
use CGI::Session qw/-ip_match/;
use IO::File;
use IO::String;
use CGI::Cookie qw();
use DBI;
use DBD::Oracle;
use Socket;

require "ctime.pl";
#
     my ($sec, $min, $hour, $day, $mon, $year) = localtime time;
     my $rightnow = " ";
     if ((defined $sec) and (defined $min) and (defined $hour) and (defined $day) and (defined $mon) and (defined $year)) {
        $mon++;                         # month starts at base zero in Perl, so add 1
        $year += 1900;
        if (length($min) == 1) {
          $min = "0" . $min;
          }
        $rightnow = "$day $mon $year  $hour:$min - ";
        }


BEGIN {
  $ENV{'ORACLE_HOME'} = '/oracle/app/oracle/product/10.2.0/db_1';
}


my $ora_connect = "dbi:Oracle:host=130.159.235.34;port=1521;SID=VGER";
my $ora_user = "ro_strathdb";
my $ora_pw = "ro_strathdb";
my $logstring = "";			# character string will be appended to, and written out at end of program
my $emailflag = 0;			# $logstring will be emailed as well if Bad Things happen (flag is 1)
my $failed_update = 'F';		# flag in event of failure - initially False
my $q= new CGI;

print $q->header();
my $s = CGI::Session->load($q);

my @xml= $q->param('POSTDATA'); #get the POST data sent by WPM and
my $jxml = join('', @xml);		#convert it from an array into a string var
my $parser = XML::LibXML->new();			#initialise the XML parser
my $xmlstr = $parser->parse_string($jxml);	#and feed in the WPM XML post
my $unform_patr_id;
my $unform_fname;
my $unform_sname;
my $forenames;
my $surname;
my $debt = 0;
my $sum_paid;
my $currencyType = "GBP";
my $date_paid = strftime '%Y%m%d    %H%M%S', localtime;
my $feetype ;
my $paymentType;
my $feeAmount;
my $instId;
my $patronid;
my $receipt;

#extract the message id from the returned XML
foreach my $msgidnode($xmlstr->findnodes('wpmpaymentrequest/@msgid')){
	$patronid=$msgidnode->nodeValue;
	}

#extract the institution id from the returned XML
foreach my $idnode($xmlstr->findnodes('wpmpaymentrequest/customerid')){
	$unform_patr_id=$idnode->toString();
	}
	
#extract the amount owed from the returned XML
foreach my $debtnode($xmlstr->findnodes('*/*/*[@payid]/amounttopay')){
	$debt = $debt+$debtnode->textContent;
	}
	
#extract the amount paid from the returned XML
foreach my $paynode($xmlstr->findnodes('*/transaction/totalpaid')){
	$sum_paid = $paynode->textContent;
	}
	
#extract the reference from the returned XML
foreach my $receiptnode($xmlstr->findnodes('*/transaction/transid')){
	$receipt = $receiptnode->toString();
	}
	
#extract the forename from the returned XML
foreach my $forenamesnode($xmlstr->findnodes('wpmpaymentrequest/firstname')){
	$unform_fname=$forenamesnode->toString();
	}
	
#extract the surname from the returned XML
foreach my $surnamenode($xmlstr->findnodes('wpmpaymentrequest/lastname')){
	$unform_sname=$surnamenode->toString();
	}

#remove the CDATA tags from the xml fields to allow their use
($forenames)=$unform_fname=~ /CDATA\[(.*)\]\]/g;
($surname)=$unform_sname=~ /CDATA\[(.*)\]\]/g;
($patronid)=$unform_patr_id=~ /CDATA\[(.*)\]\]/g;

if ((defined $patronid) and (defined $surname)and (defined $forenames)and (defined $debt)  ){$logstring = $rightnow . ", LIVE, " . $date_paid . ", " . $surname . ", " . $forenames . ", " . $patronid . ", " . $receipt;			# pure log line without extra text, for reports

  my ($status, $logstring_out, $emailflag, $npatid, $nbcode, $nsumpaid, $nreceipt) = process_patron($surname, $patronid, $debt, $sum_paid, $receipt, $logstring, $emailflag);
  }

# start process_patron subroutine
sub process_patron {

  my $surname = shift;
  my $patronid = shift;
  my $debt = shift;
  my $sum_paid = shift;
  my $receipt = shift;
  my $logstring = shift;
  my $emailflag = shift;
  # normalize values
  my $norm_surname = uc $surname;
  $norm_surname =~ s/[^A-Z -]//g;      # remove anything that isn't alphabetic, a hyphen, or a space
  $norm_surname =~ s/-/ /g;
  $norm_surname =~ s/\s+/ /g;
  $norm_surname = substr($norm_surname, 0, 30);  	# NOTE: surname limited to 30 characters

  my $norm_patronid = $patronid;
  if (defined $norm_patronid) {
    $norm_patronid =~ s/[^0-9]//g;			# numeral, not string, but both behave the same way
    }							# end loop if defined
  my $norm_debt = $debt;
  if (defined $norm_debt) {
    $norm_debt =~ s/[^0-9]//g;				# no decimal point?
    # $norm_debt = sprintf("%.2f", $norm_debt);		# put decimal point back
    }							# end loop if defined
  my $norm_sum_paid = $sum_paid;
  if (defined $norm_sum_paid) {
    # $norm_sum_paid =~ s/[^0-9]//g;			# no decimal point?
    $norm_sum_paid =~ s/[^0-9\.]//g;			# include decimal point (imprecise abou point position but probably harmless)
    my $paid_div_100 = $norm_sum_paid/100;		# divide by 100 as a first step to re-inserting the decimal point
    my $paid_sprint_only = sprintf("%.2f", $norm_sum_paid);	# for information
    # $norm_sum_paid = sprintf("%.2f", $norm_sum_paid/100);	# put back decimal point and divide by 100
    $norm_sum_paid = sprintf("%.2f", $norm_sum_paid);		# put back decimal point only or at least adjust spacing
    $logstring = $logstring . ", " . $norm_sum_paid . " ";
    }                                                   # end loop if defined
  my $norm_receipt = $receipt;
  if (defined $norm_receipt) {
  $norm_receipt =~ s/[^A-Z0-9]//g;			# Allow for letters as well as numbers
    }                                                   # end loop if defined
		# $logstring remains tainted; $emailflag is currently zero

		
  # open database connection
  my $dbh = DBI->connect($ora_connect, $ora_user, $ora_pw);

  my $result = "";
  if (defined $norm_patronid) {
    if ($norm_patronid > 0) {

       ### open temporary log to check on calculations
      open (TEMPLOG1, ">>/m1/voyager/strathdb/local/payment/templog.txt") || warn "cannot append to /m1/voyager/strathdb/local/payment/templog.txt: $!";
      print TEMPLOG1 "$logstring, ";

      my ($last_name, $institution_id, $totfees) = find_patron($dbh, $norm_patronid);

      my ($patrid) = fetch_patrid($dbh, $norm_patronid);

      my ($barcode) = fetch_barcode($dbh, $patrid);


	# find total fees for a check, and barcode_status
	# $norm_receipt sould NOT be in the file of receipts
	# $totfees should be the same as $norm_debt*100 and $norm_sum_paid*100
        if (($totfees/100) == $norm_sum_paid) {
          print TEMPLOG1 "total fees: $totfees OK, ";
          }
        elsif ($totfees == $norm_sum_paid) {
          print TEMPLOG1 "totfees: $totfees - $norm_sum_paid in pence, "
          }
        else {
          print TEMPLOG1 "total fees: $totfees *not* OK!!!, ";
          }
	# $patron_id should be defined and greater than zero
	# $barcode_status should be defined and a value from 1 to 5 - Active lost stolen expired other.

            $result = update_patron($norm_sum_paid, $norm_patronid, $barcode);

      # subroutine to update Voyager with the paid amount $norm_sum_paid  using SIP2
      # and parse the returned message Y (successful) or N (unsuccessful)
	if($result !=1){
		#insert db and log update code here
		}

      		# subroutine return value indicates this - append to logfile
      		# if unsuccessful then raise email flag
      print TEMPLOG1 "\n";				# line feed - end one line in TEMPLOG1
      close (TEMPLOG1);					# close TEMPLOG1 to be polite

      }							# end loop if $norm_patronid > 0
    }							# end loop if defined $norm_patronid 

  return ($result, $logstring,  $emailflag, $norm_patronid, $norm_sum_paid, $norm_receipt);
  }				# end subroutine process_patron

# start fetch_patrid subroutine
  sub fetch_patrid {

  	  my ($dbh, $patronid) = @_;
	  my $patsql = qq(select patron_id from patron where institution_id = '$patronid');
	  my $sth = $dbh->prepare($patsql);
	  $sth->execute();
	  if (0 == $sth->rows) {
		    my @row = $sth->fetchrow_array;
		    return @row;
		}
	  else {
	  	return (undef);
	  }
}				# end of subroutine fetch_patrid

# start fetch_barcode subroutine
  sub fetch_barcode {
	  my ($dbh, $patrid) = @_;
	  my $patsqlb = qq(select PATRON_BARCODE from PATRON_BARCODE where patron_id = $patrid and  BARCODE_STATUS = '1');
	  my $sth = $dbh->prepare($patsqlb);
	  $sth->execute();
	  if (0 == $sth->rows) {
		    my @barcode = $sth->fetchrow_array;

		    return @barcode;
	  } 
	 else {
	    return (undef);
	  }
}				# end of subroutine fetch_barcode

# start find_patron subroutine
sub find_patron {

  my ($dbh, $patronid) = @_;
  my $patsql = qq(
select unique p.last_name, p.institution_id, p.total_fees_due
from patron p
where p.institution_id = '$patronid'
);
  my $sth = $dbh->prepare($patsql);
  $sth->execute();
  if (0 == $sth->rows) {
    my @row = $sth->fetchrow_array;
    return @row;
  } else {
    return (undef, undef, undef);
  }

}				# end of subroutine find_patron



sub update_patron {

  my ($sum_paid, $norm_patronid, $barcode) = @_;

my $DEBUG = 0;
my $Port = 7031;
my $Host = inet_aton('pumblechook.lib.strath.ac.uk');
my $User = "webpay";
my $Pass = "D3cember!";
my $Loc  = "EPAYMENTS";

open (LOGFILE1, ">>/m1/voyager/strathdb/local/payment/updatelog.txt") || warn "cannot append to /m1/voyager/strathdb/local/payment/updatelog.txt: $!";

my $yr = sprintf("%d", $year);
my $mn = sprintf("%d", $mon);
my $dy = sprintf("%d", $day);
my $hou = sprintf("%d", $hour);
my $minu = sprintf("%d", $min);
my $seco = sprintf("%d", $sec);


                 # pad out short numbers so they always take 2 characters
if (length($seco) == 1) {
   $seco = "0" . $seco;
   }
if (length($minu) == 1) {
   $minu = "0" . $minu;
   }
if (length($hou) == 1) {
   $hou = "0" . $hou;
   }
if (length($dy) == 1) {
   $dy = "0" . $dy;
   }
if (length($mn) == 1) {
   $mn = "0" . $mn;
   }

my $ymdtime = $yr . $mn . $dy . "    " . $hou . $minu . $seco;		# concatenate variables

# Change as needed
# 63 is followed by 001 language, YYYYMMDDZZZZHHMMSS
# Barry's initial values     6300120100325    121500   for 2010 03 25 at 12:15:00
# then 'summary' which is blank except for Y in 3rd place (charged items) or 4th (fine items)
#   note - AA is followed by barcode
#   note on Perl - $DEBUG && print tests for $DEBUG still being 0 (no problems) before print.
# experiment with fine paid message - BV is followed by amount in pence.
# date + 4 spaces 'ZZZZ' + time + '01' (payment) + '02' (payment type 'Credit/debit card').
# my @stanza = ("3720100922    1215000102GBP|BV5.00|AO|AA999999999|\r");

#my @stanza = ("37".$ymdtime."0100GBP|BV".$sum_paid."|AO|AA".$norm_patronid."|\r");
my @stanza = ("37".$ymdtime."0100GBP|BV".$sum_paid."|AO|AA".$barcode."|\r");
print Dumper(@stanza)
# Do NOT Change
my $LoginBuffer = "9300|CN$User|CO$Pass|CP$Loc\r";
my $LoginBuffer2 = "9900802.00\r";

# Initialise variable
my $bufferR = "";


# Open the Socket & connect to SelfCheck
$DEBUG && print LOGFILE1 "BEGIN socket creation\n";
socket(SH, PF_INET, SOCK_STREAM, getprotobyname('tcp')) || die $!;
my $sin = sockaddr_in ($Port,$Host);
connect(SH,$sin) || die $!;
$DEBUG && print LOGFILE1 "END socket creation\n";

print LOGFILE1 "BEGIN Login\n";
print LOGFILE1 " SENDING:        ";
print LOGFILE1 $LoginBuffer;
syswrite(SH, $LoginBuffer, length($LoginBuffer));
print LOGFILE1 "\n";
print LOGFILE1 " RECEIVING:      ";
sysread(SH, $bufferR, 2000); # read at most 2000 bytes from SH
print LOGFILE1 $bufferR; print LOGFILE1 "\n";

$DEBUG && print LOGFILE1 "BEGIN sleep 10\n";
$DEBUG && sleep 10;
$DEBUG && print LOGFILE1 "END sleep 10\n";


print LOGFILE1 " SENDING:        "; 
print LOGFILE1 $LoginBuffer2;
syswrite(SH, $LoginBuffer2, length($LoginBuffer2));
print LOGFILE1 "\n";
print LOGFILE1 " RECEIVING:      ";
sysread(SH, $bufferR, 2000); # read at most 2000 bytes from SH
print LOGFILE1 $bufferR; print "\n";
print LOGFILE1 "\n";
$DEBUG && print LOGFILE1 "BEGIN sleep 10\n";
$DEBUG && sleep 10;
$DEBUG && print LOGFILE1 "END sleep 10\n";
print LOGFILE1 "END Login\n";

print LOGFILE1 "\n";

print LOGFILE1 "\n";

my $ind = 0;
my $count = scalar(@stanza);
print LOGFILE1 "BEGIN Stanza Loop";
while ( $ind != $count ) {
        print LOGFILE1 "\n";
        print LOGFILE1 " SENDING:        "; print LOGFILE1  $stanza[$ind];
        syswrite(SH, $stanza[$ind], length($stanza[$ind]));
        print LOGFILE1 "\n";
        print LOGFILE1 " RECEIVING:      ";
        sysread(SH, $bufferR, 2000); # read at most 2000 bytes from SH
        print LOGFILE1 $bufferR; print LOGFILE1 "\n";
        $ind++;
};
print LOGFILE1 "END Stanza Loop\n";
print LOGFILE1 "loop count ind is: $ind \n";
# parse $bufferR to see if the update was successful

# Close the socket
$DEBUG && print LOGFILE1 "BEGIN Socket Close\n";
close(SH);
$DEBUG && print LOGFILE1 "END Socket Close\n";

close (LOGFILE1);

# parse $bufferR to see if the update was successful
my ($answer1) = substr($bufferR, 2, 1);
if($answer1 =='N'){

#if update was unsuccessful, write this out to a log file for manual processing.
open (UPDATEFILE1, ">>/m1/voyager/strathdb/local/payment/failed_updates.txt") || warn "cannot append to /m1/voyager/strathdb/local/payment/failed_updates.txt: $!";
print UPDATEFILE1 "Time of payment(YYYYMMDD HHMMSS):".$ymdtime.", Amount Paid:".$sum_paid.", Institution ID:".$norm_patronid."\n";
close (UPDATEFILE1);

}
return ($answer1);



}