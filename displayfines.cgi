#!/m1/shared/bin/perl

use strict;
use warnings;

use CGI;
use CGI::Carp;
use CGI::Session;
use DBI;
use DBD::Oracle;
use XML::Simple;
use Data::Dumper;
use CGI::Cookie;
use CGI qw/:standard/;

#file will not work without session declaration as first line
my $q = CGI->new();
my $s = CGI::Session->new($q);
print $s->header();

my $filen;
my $hash_ref;
my $surname;
my $inst_no;
my @HoA;
my @arr;
my $fftype;


#test if user is logged in to Voyager. Need to add redirect to this.
$s->param('test', $q->param('test'));
$filen= $q->escapeHTML($s->param('test'));
open my $fh, '<', "/pds_gan/pds_files/z312.$filen" or die "You need to log in to My Account first.  Add handler to this $!\n";

my %hash = map { split /=\s+/; } <$fh>;
close $fh;

while (my ($key, $val) = each %hash) {
if($key =~ 'voyager_ldap_id'){
		chomp($val);
		$surname = $val;
   }
if($key =~ 'voyager_ldap_pass'){
		chomp($val);
		$inst_no = $val;
   }

}
BEGIN {
  $ENV{'ORACLE_HOME'} = '/oracle/app/oracle/product/11.2.0/db_1';
}
my $ora_connect = "dbi:Oracle:host=130.159.235.34;port=1521;SID=VGER";
my $ora_user = "ro_strathdb";
my $ora_pw = "ro_strathdb";
my $login_page_template = "/m1/voyager/strathdb/local/circ-history/payment-login.html";
my $patsql;
my $patron_title;
my $patron_id;
my $norm_surname;
my $norm_patron_id;
my $norm_barcode;
my $rowtot;
my $barcode;
my $patron_email;

# Message to display in title field if item or bib record is no longer in database
my $missing_title = "Title no longer in catalogue.";

# HTML templates
my $login_page_try_again_template = "/m1/voyager/strathdb/local/circ-history/payment-login-tryagain.html";
my $patron_name_header_template = "/m1/voyager/strathdb/local/circ-history/payment-patron-header.html";
my $patron_name_header_nopay_template = "/m1/voyager/strathdb/local/circ-history/payment-patron-header-nopay.html";
my $patron_name_header_CreCon_template = "/m1/voyager/strathdb/local/circ-history/payment-patron-header-Cre.html";
my $patron_name_header_LosStol_template = "/m1/voyager/strathdb/local/circ-history/payment-patron-header-Los.html";
my $patron_name_footer_template = "/m1/voyager/strathdb/local/circ-history/payment-patron-footer.html";
my $ff_debt_header_template = "/m1/voyager/strathdb/local/circ-history/payment-debts-header.html";
my $ff_debt_entry_template = "/m1/voyager/strathdb/local/circ-history/payment-debts-entry.html";
my $ff_debt_entry_template_no_hotlink = "/m1/voyager/strathdb/local/circ-history/payment-debts-entry-no-hotlink.html";
my $ff_debt_footer_template = "/m1/voyager/strathdb/local/circ-history/payment-debts-footer.html";
my $current_charges_header_template = "/m1/voyager/strathdb/local/circ-history/payment-currentcharges-header.html";
my $current_charges_entry_template = "/m1/voyager/strathdb/local/circ-history/payment-currentcharges-entry2.html";
my $current_charges_entry_template_Odue = "/m1/voyager/strathdb/local/circ-history/payment-currentcharges-entry3.html";
my $current_charges_entry_template_no_hotlink = "/m1/voyager/strathdb/local/circ-history/payment-currentcharges-entry2-no-hotlink.html";
my $current_charges_footer_template = "/m1/voyager/strathdb/local/circ-history/payment-currentcharges-footer.html";

# URLs for WPM to use
my $callbackurl = "http://pumblechook.lib.strath.ac.uk:7004/cgi-bin/payment_update.cgi";	# background cancellation of debt
my $cancelurl = "http://pumblechook.lib.strath.ac.uk:7004/cgi-bin/libdebt.cgi";		# back to web payment login page
my $redirecturl = "http://pumblechook.lib.strath.ac.uk:7004/cgi-bin/lib-lookup.cgi";		# after finish payment


$inst_no =~ s/ //g;
retrieve_patron_id();
emit_circ_history();

#returns patron id for the logged in patron
sub retrieve_patron_id{

	my $dbh = DBI->connect($ora_connect, $ora_user, $ora_pw, { RaiseError => 1 });
	$patsql = qq(
		select p.PATRON_ID
		from PATRON p 
		where p.INSTITUTION_ID = '$inst_no'
		);
	
	my $sth = $dbh->prepare($patsql);
	$sth->execute();
	if (0 == $sth->rows) {
       $patron_id = $sth->fetchrow_array;
	   return $patron_id;
	} 
	$dbh->disconnect();
}

sub emit_circ_history {

  # normalize values
  $norm_surname = uc $surname;
  $norm_surname =~ s/[^A-Z -]//g;      # remove anything that isn't alphabetic, a hyphen, or a space
  $norm_surname =~ s/-/ /g;
  $norm_surname =~ s/\s+/ /g;
  $norm_surname = substr($norm_surname, 0, 30);
  $norm_surname =~ s/ //g;
  $norm_patron_id =  $patron_id;               # normalise barcode to upper case
  $norm_patron_id =~ s/[^A-Z0-9]//g;              # accepts A-Z, 0-9, discards spaces and other characters

  # open database connection
  my $dbh = DBI->connect($ora_connect, $ora_user, $ora_pw,{ RaiseError => 1 });

  my $retval = 0;

  my ($patron_title, $patron_id, $first_name, $middle_name, $last_name, $cloans, $totfees, $patron_email) = find_patron($dbh, $norm_surname, $norm_barcode);
    if (defined $middle_name) {
    if (length($middle_name) > 0) {
      $first_name = $first_name . " " . $middle_name;
	
      }                                         # end loop to combine middle and first if a middle name exists
    }
  if (defined $totfees) {
    if ($totfees > 0) {

      $totfees = sprintf("%.2f", $totfees/100);
      }						# end inner loop to format number to 2 decimal places if it exists
    }						# end outer loop to format number if defined (not null)
#   at this point insert get_ff_details (originally mostly from emit_ff_details) so that the ledger code amounts can be included
#   in the header - process will duplicate emit_ff_details which is absurd but seems simpler - minimum data needed.
#   get_ff_details can only return data if a patron id exists, so it comes within the following loop.
if (defined $patron_id) {
    my ($str_fine,$str_lost, $STR_LSTITEM, $str_medBkngLate, $str_medbkngUsg, $str_eqpRpl, $str_ovdSer, $str_accrFine, $str_accrDem, $str_demerit,
 $str_damaged, $str_chargCarFwd,$str_chrgcarfwd2, $str_cargcarfwd3, $str_chargsCarFwd,  $str_cargscarfwd3, $str_lODPricingLtr, $str_pricedLostBk, $str_invWrOffFinance,
 $str_admFeeInv, $str_admnChar, $str_postage, $str_prtRet, $str_rebind, $str_VAT, $str_laminate, $str_ILLFee, $str_sysTest ,$rowtot) = get_ff_details($dbh, $patron_id);

 emit_patron_name_header($first_name, $last_name, $norm_surname, $patron_id, $cloans, $totfees, $str_fine, $str_lost, $STR_LSTITEM, $str_medBkngLate, $str_medbkngUsg, $str_eqpRpl, $str_ovdSer, $str_accrFine, $str_accrDem, $str_demerit,
 $str_damaged, $str_chargCarFwd,$str_chrgcarfwd2, $str_cargcarfwd3, $str_chargsCarFwd,  $str_cargscarfwd3, $str_lODPricingLtr, $str_pricedLostBk, $str_invWrOffFinance,
 $str_admFeeInv, $str_admnChar, $str_postage, $str_prtRet, $str_rebind, $str_VAT, $str_laminate, $str_ILLFee, $str_sysTest, $rowtot, $patron_title, $patron_email, $inst_no);

 emit_ff_details($dbh, $patron_id);

require "ctime.pl";
     my ($sec, $min, $hour, $day, $mon, $year) = localtime time;
	 my $rightnow = " ";
     if ((defined $sec) and (defined $min) and (defined $hour) and (defined $day) and (defined $mon) and (defined $year)) {

        $mon++;                         # month starts at base zero in Perl, so add 1
        $year += 1900;
        $rightnow = "$day $mon $year  $hour:$min ";
        }
	open LOGFILE, '>>/m1/voyager/strathdb/local/payment/paylookup.txt' || warn "cannot append to /m1/voyager/strathdb/local/payment/paylookup.txt: $!";
    print LOGFILE "$rightnow, $patron_id, $first_name, $last_name, $cloans, $totfees, $str_fine,$str_lost, $STR_LSTITEM, $str_medBkngLate, $str_medbkngUsg, $str_eqpRpl, $str_ovdSer, $str_accrFine, $str_accrDem, $str_demerit, $str_damaged, $str_chargCarFwd,$str_chrgcarfwd2, $str_cargcarfwd3, $str_chargsCarFwd,  $str_cargscarfwd3, $str_lODPricingLtr, $str_pricedLostBk, $str_invWrOffFinance, $str_admFeeInv, $str_admnChar, $str_postage, $str_prtRet, $str_rebind, $str_VAT, $str_laminate, $str_ILLFee, $str_sysTest,$rowtot \n";
    close LOGFILE;
}
else {
    $retval = 1; # could not find patron
  }

  # close connection
  $dbh->disconnect();

  return $retval;
}

sub find_patron {

  my ($dbh, $surname, $barcode) = @_;
  my $patsql = qq(
select p.title, p.patron_id, p.first_name, p.middle_name, p.last_name, p.current_charges, p.total_fees_due, a.address_line1
from patron p 
join patron_address a
on p.patron_id = a.patron_id
where p.normal_last_name ='$norm_surname'
and p.patron_id = $patron_id
and ADDRESS_TYPE= 3
);
  my $sth = $dbh->prepare($patsql);
  $sth->execute();
  if (0 == $sth->rows) {
    my @row = $sth->fetchrow_array;
    return @row;
  } else {
    return (undef, undef,undef, undef, undef, undef, undef, undef);
  }
}

sub get_ff_details {
  my $dbh = shift;
  my $patron_id = shift;
  $patron_id  =~ s/ //g;

  my $ffsql = qq(
select ff.fine_fee_id, ff.item_id, to_char(ff.create_date, 'DD-MON-YYYY'), ff.fine_fee_balance, ff.fine_fee_type, ff.fine_fee_location, fft.fine_fee_desc
from   fine_fee ff, fine_fee_type fft
where  ff.patron_id = $patron_id
and    ff.fine_fee_balance > 0
and    fft.fine_fee_type = ff.fine_fee_type
order by ff.fine_fee_id, ff.create_date
);

  my $sth = $dbh->prepare($ffsql);
  $sth->execute() or print "Can't execute SQL statement: ", $sth->errstr(), "\n";

  my @fines = ();
  while (my @ctrow = $sth->fetchrow_array()) {
    push @fines, join("\t", @ctrow);
  }
my ($str_lost, $str_fine, $STR_LSTITEM, $str_medBkngLate, $str_medbkngUsg, $str_eqpRpl, $str_ovdSer, $str_accrFine, $str_accrDem, $str_demerit, $str_damaged, $str_chargCarFwd,
    $str_chrgcarfwd2, $str_cargcarfwd3, $str_chargsCarFwd, $str_cargscarfwd3, $str_lODPricingLtr, $str_pricedLostBk, $str_invWrOffFinance, $str_admFeeInv, $str_admnChar,
   $str_postage, $str_prtRet, $str_rebind, $str_VAT, $str_laminate, $str_ILLFee, $str_chrgcarfwd2, $str_cargcarfwd3, $str_chargsCarFwd, $str_sysTest);
   
	$str_lost  = $str_fine = $STR_LSTITEM = $str_medBkngLate = $str_medbkngUsg = $str_eqpRpl = $str_ovdSer = $str_accrFine = $str_accrDem = $str_demerit = $str_damaged = $str_chargCarFwd =
    $str_chrgcarfwd2 = $str_cargcarfwd3 = $str_chargsCarFwd = $str_cargscarfwd3 = $str_lODPricingLtr = $str_pricedLostBk = $str_invWrOffFinance = $str_admFeeInv = $str_admnChar =
   $str_postage = $str_prtRet = $str_rebind = $str_VAT = $str_laminate = $str_ILLFee = $str_chrgcarfwd2 = $str_cargcarfwd3 = $str_chargsCarFwd = $str_sysTest = 0;
  
  my $i = 0;
  foreach my $ff (@fines) {

    $i++;
    my ($ffid, $itemid, $finedate, $ffbalance, $fftype, $fflocid, $ffdesc) = split /\t/, $ff, -1;

	#assigns fine type to strathclyde per-finetype balance.
    if (defined $ffbalance) {
      if ($ffbalance > 0) {
        $ffbalance = sprintf("%.2f", $ffbalance/100);
	 if ($fftype eq '1') {$str_fine = $str_fine + $ffbalance;}							
        if ($fftype eq '2') {$str_lost = $str_lost + $ffbalance;}	
        if ($fftype eq '3') {$STR_LSTITEM = $STR_LSTITEM + $ffbalance;}				
	 if ($fftype eq '4') {$str_medBkngLate = $str_medBkngLate + $ffbalance;}							
        if ($fftype eq '5') {$str_medbkngUsg = $str_medbkngUsg + $ffbalance;}	
        if ($fftype eq '6') {$str_eqpRpl = $str_eqpRpl + $ffbalance;}							
        if ($fftype eq '7') {$str_ovdSer = $str_ovdSer + $ffbalance;}							
        if ($fftype eq '8') {$str_accrFine = $str_accrFine + $ffbalance;}	
        if ($fftype eq '9') {$str_accrDem = $str_accrDem + $ffbalance;}				
	 if ($fftype eq '10') {$str_demerit = $str_demerit + $ffbalance;}							
        if ($fftype eq '11') {$str_damaged = $str_damaged + $ffbalance;}	
        if ($fftype eq '12') {$str_chargCarFwd = $str_chargCarFwd + $ffbalance;}							
        if ($fftype eq '13') {$str_chrgcarfwd2 = $str_chrgcarfwd2 + $ffbalance;}							
        if ($fftype eq '14') {$str_cargcarfwd3 = $str_cargcarfwd3 + $ffbalance;}	
        if ($fftype eq '15') {$str_chargsCarFwd = $str_chargsCarFwd + $ffbalance;}				
	 if ($fftype eq '16') {$str_cargscarfwd3 = $str_cargscarfwd3 + $ffbalance;}							
        if ($fftype eq '17') {$str_lODPricingLtr = $str_lODPricingLtr + $ffbalance;}	
        if ($fftype eq '18') {$str_pricedLostBk = $str_pricedLostBk + $ffbalance;}		
        if ($fftype eq '19') {$str_invWrOffFinance = $str_invWrOffFinance + $ffbalance;}							
        if ($fftype eq '20') {$str_admFeeInv = $str_admFeeInv + $ffbalance;}							
        if ($fftype eq '21') {$str_admnChar = $str_admnChar + $ffbalance;}	
        if ($fftype eq '22') {$str_postage = $str_postage + $ffbalance;}				
	 if ($fftype eq '23') {$str_prtRet = $str_prtRet + $ffbalance;}							
        if ($fftype eq '24') {$str_rebind = $str_rebind + $ffbalance;}	
        if ($fftype eq '25') {$str_VAT = $str_VAT + $ffbalance;}							
        if ($fftype eq '26') {$str_laminate = $str_laminate + $ffbalance;}							
        if ($fftype eq '27') {$str_ILLFee = $str_ILLFee + $ffbalance;}	
        if ($fftype eq '28') {$str_sysTest = $str_sysTest + $ffbalance;}				
			
        $rowtot = $rowtot + $ffbalance;
        }						# end loop if $ffbalance >0
      }							# end loop if defined $ffbalance

    }						# end loop for each fine/fee row
	
	#formats balances gathered above
  if ($str_fine > 0) {$str_fine = sprintf("%.2f", $str_fine)};
  if ($str_lost > 0) {$str_lost = sprintf("%.2f", $str_lost)};
  if ($STR_LSTITEM > 0) {$STR_LSTITEM = sprintf("%.2f", $STR_LSTITEM)};
  if ($str_medBkngLate > 0) {$str_medBkngLate = sprintf("%.2f", $str_medBkngLate)};
  if ($str_medbkngUsg > 0) {$str_medbkngUsg = sprintf("%.2f", $str_medbkngUsg)};
  if ($str_eqpRpl > 0) {$str_eqpRpl = sprintf("%.2f", $str_eqpRpl)};
  if ($str_ovdSer > 0) {$str_ovdSer = sprintf("%.2f", $str_ovdSer)};
  if ($str_accrFine > 0) {$str_accrFine = sprintf("%.2f", $str_accrFine)};
  if ($str_accrDem > 0) {$str_accrDem = sprintf("%.2f", $str_accrDem)};
  if ($str_demerit > 0) {$str_demerit = sprintf("%.2f", $str_demerit)};
  if ($str_damaged > 0) {$str_damaged = sprintf("%.2f", $str_damaged)};
  if ($str_chargCarFwd > 0) {$str_chargCarFwd = sprintf("%.2f", $str_chargCarFwd)};
  if ($str_chrgcarfwd2 > 0) {$str_chrgcarfwd2 = sprintf("%.2f", $str_chrgcarfwd2)};
  if ($str_cargcarfwd3 > 0) {$str_cargcarfwd3 = sprintf("%.2f", $str_cargcarfwd3)};
  if ($str_chargsCarFwd > 0) {$str_chargsCarFwd = sprintf("%.2f", $str_chargsCarFwd)};
  if ($str_cargscarfwd3 > 0) {$str_cargscarfwd3 = sprintf("%.2f", $str_cargscarfwd3)};
  if ($str_lODPricingLtr > 0) {$str_lODPricingLtr = sprintf("%.2f", $str_lODPricingLtr)};
  if ($str_pricedLostBk > 0) {$str_pricedLostBk = sprintf("%.2f", $str_pricedLostBk)};
  if ($str_invWrOffFinance > 0) {$str_invWrOffFinance = sprintf("%.2f", $str_invWrOffFinance)};
  if ($str_admFeeInv > 0) {$str_admFeeInv = sprintf("%.2f", $str_admFeeInv)};
  if ($str_admnChar > 0) {$str_admnChar = sprintf("%.2f", $str_admnChar)};
  if ($str_postage > 0) {$str_postage = sprintf("%.2f", $str_postage)};
  if ($str_prtRet > 0) {$str_prtRet = sprintf("%.2f", $str_prtRet)};
  if ($str_rebind > 0) {$str_rebind = sprintf("%.2f", $str_rebind)};
  if ($str_VAT > 0) {$str_VAT = sprintf("%.2f", $str_VAT)};
  if ($str_laminate > 0) {$str_laminate = sprintf("%.2f", $str_laminate)};
  if ($str_ILLFee > 0) {$str_ILLFee = sprintf("%.2f", $str_ILLFee)};
  if ($str_sysTest > 0) {$str_sysTest = sprintf("%.2f", $str_sysTest)};
  if ($rowtot > 0) {$rowtot = sprintf("%.2f", $rowtot )};
  return ($str_fine,$str_lost, $STR_LSTITEM, $str_medBkngLate, $str_medbkngUsg, $str_eqpRpl, $str_ovdSer, $str_accrFine, $str_accrDem, $str_demerit,
 $str_damaged, $str_chargCarFwd,$str_chrgcarfwd2, $str_cargcarfwd3, $str_chargsCarFwd,$str_cargscarfwd3, $str_lODPricingLtr, $str_pricedLostBk, $str_invWrOffFinance,
 $str_admFeeInv, $str_admnChar, $str_postage, $str_prtRet, $str_rebind, $str_VAT, $str_laminate, $str_ILLFee, $str_sysTest ,$rowtot);

}                               # END of sub get_ff_details

sub emit_patron_name_header {

  my ( $first_name, $last_name, $norm_surname, $patron_id, $cloans, $totfees, $str_fine,$str_lost, $STR_LSTITEM, $str_medBkngLate, $str_medbkngUsg, $str_eqpRpl, $str_ovdSer, $str_accrFine, $str_accrDem, $str_demerit,
 $str_damaged, $str_chargCarFwd,$str_chrgcarfwd2, $str_cargcarfwd3, $str_chargsCarFwd,  $str_cargscarfwd3, $str_lODPricingLtr, $str_pricedLostBk, $str_invWrOffFinance,
 $str_admFeeInv, $str_admnChar, $str_postage, $str_prtRet, $str_rebind, $str_VAT, $str_laminate, $str_ILLFee, $str_sysTest, $rowtot, $patron_title, $patron_email, $inst_no) = @_;
  my ($itemn) = 'items';                                # define a string for plural items
  if (defined $cloans) {
    if ($cloans == 1) {
      $itemn = 'item';                                  # define the string as singular if only one item
      }
    }

  if ($totfees > 1) {


    open TEMPLATE, "<$patron_name_header_template";
    my @lines = <TEMPLATE>;
	
	
    for (my $i = 0; $i <= $#lines; $i++) {
      $lines[$i] =~ s/\$TITLE/$patron_title/g;
      $lines[$i] =~ s/\$FORENAMES/$first_name/g;
      $lines[$i] =~ s/\$LASTNAME/$last_name/g;
      $lines[$i] =~ s/\$CURRENT_LOANS/$cloans/g;
      $lines[$i] =~ s/\$EMAIL/$patron_email/g;
      $lines[$i] =~ s/\$INST/$inst_no/g;
      $lines[$i] =~ s/\$ITEMS/$itemn/g;
      $lines[$i] =~ s/\$TOTAL_FEES/$totfees/g;
      $lines[$i] =~ s/\$LOGIN_SURNAME/$norm_surname/g;
      $lines[$i] =~ s/\$PATRONID/$patron_id/g;  
      $lines[$i] =~ s/\$STR_FINE/$str_fine/g;
      $lines[$i] =~ s/\$STR_LOST/$str_lost/g;
      $lines[$i] =~ s/\$STR_LSTITEM/$STR_LSTITEM/g;
      $lines[$i] =~ s/\$STR_MEDBKNGLATE/$str_medBkngLate/g;
      $lines[$i] =~ s/\$STR_MEDBKNGUSG/$str_medbkngUsg/g;
      $lines[$i] =~ s/\$STR_EQPRPL/$str_eqpRpl/g;
      $lines[$i] =~ s/\$STR_OVDSER/$str_ovdSer/g;
      $lines[$i] =~ s/\$STR_ACCRFINE/$str_accrFine/g;
      $lines[$i] =~ s/\$STR_ACCRDEM/$str_accrDem/g;
      $lines[$i] =~ s/\$STR_DEMERIT/$str_demerit/g;
      $lines[$i] =~ s/\$STR_DAMAGED/$str_damaged/g;
      $lines[$i] =~ s/\$STR_CHARGCARFWD/$str_chargCarFwd/g;
      $lines[$i] =~ s/\$STR_CHRGCARFWD2/$str_chrgcarfwd2/g;	  
      $lines[$i] =~ s/\$STR_CARGCARFWD3/$str_cargcarfwd3/g;
      $lines[$i] =~ s/\$STR_CHARGSCARFWD/$str_chargsCarFwd/g;
      $lines[$i] =~ s/\$STR_CARGSCARFWD3/$str_cargscarfwd3/g;
      $lines[$i] =~ s/\$STR_LODPRICINGLTR/$str_lODPricingLtr/g;
      $lines[$i] =~ s/\$STR_PRICEDLOSTBK/$str_pricedLostBk/g;
      $lines[$i] =~ s/\$STR_INVWROFFFINANCE/$str_invWrOffFinance/g;
      $lines[$i] =~ s/\$STR_ADMFEEINV/$str_admFeeInv/g;
      $lines[$i] =~ s/\$STR_ADMNCHAR/$str_admnChar/g;
      $lines[$i] =~ s/\$STR_POSTAGE/$str_postage/g;
      $lines[$i] =~ s/\$STR_PRTRET/$str_prtRet/g;
      $lines[$i] =~ s/\$STR_REBIND/$str_rebind/g;
      $lines[$i] =~ s/\$STR_VAT/$str_VAT/g;
      $lines[$i] =~ s/\$STR_LAMINATE/$str_laminate/g;
      $lines[$i] =~ s/\$STR_ILLFEE/$str_ILLFee/g;
      $lines[$i] =~ s/\$STR_SYSTEST/$str_sysTest/g;
      $lines[$i] =~ s/\$CALLBACKURL/$callbackurl/g;
      $lines[$i] =~ s/\$CANCELURL/$cancelurl/g;
      $lines[$i] =~ s/\$REDIRECTURL/$redirecturl/g;

	  
    }								# end of loop for each line if money can be paid
    print join("", @lines);
    close TEMPLATE;
  }								# end of loop if money can be paid
                                                    # end ELSIF loop for Credit Control flag
                                                            # end ELSIF loop where barcode status is Lost or Stolen
  else {							# start ELSE loop if no money can be paid
    open TEMPLATE, "<$patron_name_header_nopay_template";
    my @lines = <TEMPLATE>;
    for (my $i = 0; $i <= $#lines; $i++) {			# loop for each line
      $lines[$i] =~ s/\$FORENAMES/$first_name/g;
      $lines[$i] =~ s/\$LASTNAME/$last_name/g;
      $lines[$i] =~ s/\$CURRENT_LOANS/$cloans/g;
      $lines[$i] =~ s/\$ITEMS/$itemn/g;
      $lines[$i] =~ s/\$TOTAL_FEES/$totfees/g;
      $lines[$i] =~ s/\$LOGIN_SURNAME/$norm_surname/g;
    }								# end loop for each line if no money can be paid
    print join("", @lines);
    close TEMPLATE;
  }								# end ELSE loop if no money can be paid
}		

sub emit_ff_details {
  my $dbh = shift;
  my $patron_id = shift;

  my $finesql = qq(
select ff.fine_fee_id, ff.item_id, to_char(ff.create_date, 'DD-MON-YYYY'), ff.fine_fee_balance, ff.fine_fee_type, ff.fine_fee_location, ff.fine_fee_note, fft.fine_fee_desc
from   fine_fee ff
join fine_fee_type fft
on fft.fine_fee_type=ff.fine_fee_type
where  ff.patron_id = $patron_id
and    ff.fine_fee_balance > 0
and    fft.fine_fee_type = ff.fine_fee_type
order by ff.item_id, ff.create_date
);

  my $sth = $dbh->prepare($finesql);
  $sth->execute() or print "Can't execute SQL statement: ", $sth->errstr(), "\n";
  
  my @fines = ();
  while (my @ctrow = $sth->fetchrow_array()) {
    push @fines, join("\t", @ctrow);
	
  }

  emit_ff_details_header($patron_id);			# display header before loop to display each debt

  my $i = 0;
  foreach my $ff (@fines) {
    $i++;
    my ($ffid, $itemid, $finedate, $ffbalance, $fftype, $fflocid, $ffnote, $ffdesc) = split /\t/, $ff, -1;
    my ($title) = "";
    my ($author) = "";
    my ($imprint) = "";
    my ($bibid) = "";
    my ($location) = "";
    my ($callno) = "";
    my ($barcode) = "";

    if (defined $ffbalance) {
      if ($ffbalance > 0) {
        $ffbalance = sprintf("%.2f", $ffbalance/100);


        }                                         # end inner loop to format number to 2 decimal places if it exists
      }                                           # end outer loop to format number if defined (not null)
    if (defined $itemid) {				# loop to test item ID before getting bib details
        if ($itemid > 0) {
          ($title, $author, $imprint, $bibid, $location, $callno, $barcode) = get_bib_info($dbh, $itemid);
          }						# end inner loop if item ID is more than zero
       }						# end outer loop if item ID is defined
						# insert a space if barcode is empty
    if (defined $barcode) {				# this loop added just in case
       if ($barcode eq "") {				# null string
          $barcode = " ";				# replace null string with space
          }						# end loop if null string
       }						# end loop if defined barcode
    else {
       $barcode = " ";                              	# replace undefined string with space 
       }						# end ELSE loop if undefined barcode

    if (defined $ffnote) {				# loop to replace blank title with fine_fee note
      if (defined $itemid) {
        if ($itemid == 0) {
          $title = $ffnote;
	  }						# end loop if item ID is zero
        }						# end loop if defined item ID
      else {
        $title = $ffnote;
        }						# end loop id undefined item ID
      }							# end loop if defined fine_fee note
    # my $hotlink_ff = 'F';
    # if (defined $bibid) {
    #   if ($bibid > 0) {
    #     $hotlink_ff = 'T';				# allow for hotlink if $bibid exists
    #     }
    #   }
    # if ($hotlink_ff eq 'T') {


    emit_ff_details_entry($i, $title, $author, $imprint, $bibid, $location, $callno, $barcode, $finedate, $ffbalance, $fflocid, $ffdesc, $fftype);

    #   }						# end loop if there is a fine_fee hotlink
    # else {
    #    emit_ff_details_entry_no_hotlink(($i, $title, $author, $imprint, $bibid, $location, $callno, $barcode, $finedate, $ffbalance, $fflocid, $ffdesc);
    #    } 						#end loop if there is no fine_fee hotlink

  }
  emit_ff_details_footer($patron_id);	 # formerly emit_ff_details_footer(, $email_body);
}				# END of sub emit_historical_charge_transactions

sub emit_ff_details_header() {



  my ($patronid) = @_; 
  open TEMPLATE, "<$ff_debt_header_template";
  my @lines = <TEMPLATE>;
  print join("", @lines);
  close TEMPLATE;

}

sub emit_ff_details_footer() {
	
	my %fineline;
	my @hashd=$s->param('hasha');
	print $hashd[2]|$fineline{'type'};


  open TEMPLATE, "<$ff_debt_footer_template";
  my @lines = <TEMPLATE>;
  print join("", @lines);
  close TEMPLATE;

}
#   COMMENTED OUT: uses email variables as input
# sub emit_ff_details_footer() {
#
#  my ($send_email, $email_body) = @_; 
#  open TEMPLATE, "<$ff_debt_footer_template";
#  my @lines = <TEMPLATE>;
#  print join("", @lines);
#  close TEMPLATE;
#
# }

sub emit_ff_details_entry {
my $email;
 my %fineline;
my $i;
  my ($seq, $title, $author, $imprint, $bibid, $location, $callno, $barcode, $finedate, $ffbalance, $ffloc, $ffdesc, $fftype) = @_;
  my $hotlink_ff = 'F';

  if (defined $bibid) {
    if ($bibid > 0) {
      $hotlink_ff = 'T';                            # allow for hotlink if $bibid exists
      }
    }
  if ($hotlink_ff eq 'T') {
    open TEMPLATE, "<$ff_debt_entry_template";
    }                                               # end loop if there is a fine_fee hotlink
  else {
    open TEMPLATE, "<$ff_debt_entry_template_no_hotlink";
    }                                              #end loop if there is no fine_fee hotlink
						# open one template if there is a hotlink, another if there isn't
  my @lines = <TEMPLATE>;
  for ($i = 0; $i <= $#lines; $i++) {

    $lines[$i] =~ s/\$SEQ/$seq/g;
    $lines[$i] =~ s/\$TITLE/$title/g;
    $lines[$i] =~ s/\$AUTHOR/$author/g;
    $lines[$i] =~ s/\$IMPRINT/$imprint/g;
    $lines[$i] =~ s/\$BIBID/$bibid/g;
    $lines[$i] =~ s/\$LOCATION/$location/g;
    $lines[$i] =~ s/\$CALLNO/$callno/g;
    $lines[$i] =~ s/\$BARCODE/$barcode/g;
    $lines[$i] =~ s/\$FINEDATE/$finedate/g;
    $lines[$i] =~ s/\$FFBALANCE/$ffbalance/g;
    $lines[$i] =~ s/\$FFCODE/$ffloc/g;
    $lines[$i] =~ s/\$FFDESC/$ffdesc/g;

	}
	%fineline=('balance',$ffbalance,'type',$fftype,'date',$finedate,'barcode',$barcode);
	push (@HoA, \%fineline);
	print join("", @lines);
	$s->param(-name=>'hasha',-value=>\@HoA);
	$s->param(-name=>'fcounter',-value=>$seq);
	$s->flush();
	close TEMPLATE;
}				# END of sub emit_hist_charge_entry
				# NOTE: no bib ID in email



sub get_bib_info {
  my ($dbh, $itemid) = @_;
  
  my $title = "";
  my $author = "";
  my $imprint = "";
  my $location = "";
  my $callno = "";
  my $barcode = "";
  my $bibid = "";
					# modified 10.10.2010 to deal with cases where item ID is missing or 0
  if (defined $itemid) {
    if ($itemid gt 0) {
      my $bibsql = qq(
select bt.title, bt.author, bt.imprint, bt.bib_id
from bib_text bt, bib_item bi
where bi.item_id = $itemid
and   bi.bib_id = bt.bib_id
);
      my $sth1 = $dbh->prepare($bibsql);
      $sth1->execute();
      if (0 == $sth1->rows) {
        ($title, $author, $imprint, $bibid) = $sth1->fetchrow_array();
        }

      my $mfhdsql = qq(
select l.location_display_name, mm.display_call_no
from mfhd_master mm
join location l
on mm.LOCATION_ID = l.LOCATION_ID
join mfhd_item mi
on mi.MFHD_ID = mm.MFHD_ID
where mi.item_id = $itemid
and   mi.mfhd_id = mm.mfhd_id
and   mm.location_id = l.location_id
);
      my $sth2 = $dbh->prepare($mfhdsql);
      $sth2->execute();
      if (0 == $sth2->rows) {
        ($location, $callno) = $sth2->fetchrow_array();
        }

      my $barcodesql = qq(
select NVL(ib.item_barcode, '0')
from item_barcode ib
where ib.item_id = $itemid
);
      my $sth3 = $dbh->prepare($barcodesql);
      $sth3->execute();
      if (0 <= $sth3->rows) {
        ($barcode) = $sth3->fetchrow_array();
        }
      }                         # end loop if item ID greater than zero
    }                           # end loop if defined item ID

  $title = $missing_title unless defined $title;
  $author = "" unless defined $author;
  $imprint = "" unless defined $imprint;
  $bibid = "" unless defined $bibid;
  $location = "" unless defined $location;
  $callno = "" unless defined $callno;
  $barcode = "" unless defined $barcode;

  return ($title, $author, $imprint, $bibid, $location, $callno, $barcode);
}				# END of sub get_bib_info 

