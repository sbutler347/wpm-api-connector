#!/m1/shared/bin/perl

use XML::Writer;
use strict;
use warnings;
use IO::File;
use IO::String;
use Digest::MD5 qw(md5_hex);
use CGI; 
use XML::LibXML;
use CGI::Session qw/-ip_match/;
use LWP::UserAgent;
use Data::Dumper;
use HTTP::Request::Common;
use CGI::Cookie;
use HTTP::Cookies;


my $q = new CGI;
my $s = CGI::Session->load();
print $q->header();

#return the various values from the session as assigned by displayfines.cgi
my $output ='';
my $firstname = $q->param('FORENAMES');
my $lastname = $q->param('SURNAME');
my $title = $q->param('TITLE');
my $email = $q->param('EMAIL');
my $callback = $q->param('CALLBACKURL');
my $redirect = $q->param('REDIRECTURL');
my $cancel = $q->param('CANCELURL');
my $FINE = $q->param('36.01');
my $LOST = $q->param('36.02');
my $LSTITEM = $q->param('36.03');
my $MEDBKNGLATE = $q->param('36.04');
my $MEDBKNGUSG = $q->param('36.05');
my $EQPRPL = $q->param('36.06');
my $OVDSER = $q->param('36.07');
my $ACCRFINE = $q->param('36.08');
my $ACCRDEM = $q->param('36.09');
my $DEMERIT = $q->param('36.10');
my $DAMAGED = $q->param('36.11');
my $CHARGCARFWD = $q->param('36.12');
my $CHRGCARFWD2 = $q->param('36.13');
my $CARGCARFWD3 = $q->param('36.14');
my $CHARGSCARFWD = $q->param('36.15');
my $CARGSCARFWD3 = $q->param('36.16');
my $LODPRICINGLTR = $q->param('36.17');
my $PRICEDLOSTBK = $q->param('36.18');
my $INVWROFFFINANCE = $q->param('36.19');
my $ADMFEEINV = $q->param('36.20');
my $ADMNCHAR = $q->param('36.21');
my $POSTAGE = $q->param('36.22');
my $PRTRET = $q->param('36.23');
my $REBIND = $q->param('36.24');
my $VAT = $q->param('36.25');
my $LAMINATE = $q->param('36.26');
my $ILLFEE = $q->param('36.27');
my $SYSTEST = $q->param('36.28');
my @valu=$s->param('hasha');
my $fcount=$s->param('fcounter');
my $payid=0;
my %payablefines;
my $message;
my $cookie_jar = HTTP::Cookies->new( );
my $transactionref;
my $sharedsec;
my $clientid;
my $pathwayid;
my $deptid;
my $staffid;
my $rightnow;
my $customerid = $q->param('INST');
my $amounttopay = $q->param('TOTAL_FEES');

#write the mesgid hex for the XML POST. This needs to be appropriately completed with the sharedsecret once this is provided by WPM
my $msgid = md5_hex($customerid,$transactionref,$amounttopay,$sharedsec);


my %finesmap=("01", $FINE, "02", $LOST, "03", $LSTITEM, "04",$MEDBKNGLATE,"05",$MEDBKNGUSG,"06",$EQPRPL,"07",$OVDSER,"08",$ACCRFINE, "09",$ACCRDEM, "10",$DEMERIT,"11",$DAMAGED,
"12",$CHARGCARFWD,"13",$CHRGCARFWD2, "14", $CARGCARFWD3, "15", $CHARGSCARFWD, "16",$CARGSCARFWD3,"17", $LODPRICINGLTR,"18",$PRICEDLOSTBK,"19",$INVWROFFFINANCE,"20", $ADMFEEINV,
"21",$ADMNCHAR,"22",$POSTAGE,"23", $PRTRET,"24",$REBIND,"25",$VAT,"26",$LAMINATE,"27",$ILLFEE,"28",$SYSTEST);

while ((my $key,my $value) = each(%finesmap)){
	if($value>0){
	$payablefines{$key}=$value;
}}

require "ctime.pl";
     my ($sec, $min, $hour, $day, $mon, $year) = localtime time;

     if ((defined $sec) and (defined $min) and (defined $hour) and (defined $day) and (defined $mon) and (defined $year)) {

        $mon++;                         # month starts at base zero in Perl, so add 1
        $year += 1900;
	if(my $day <10){
		$day = "0".$day;
       	}
	if( my $month <10){
		$month = "0".$month;
       	}
	if(my $hour <10){
		$hour = "0".$hour;
       	}
	if(my $min <10){
		$min = "0".$min;
       	}
	if(my $sec <10){
		$sec = "0".$sec;
       	}
        my $rightnow = "$year-$mon-$day $hour:$min:$sec ";
        }

		#create the xml document for transmission and dump a copy to payment_xml/ 
my $doc = IO::File->new(">payment_xml/$msgid-test.xml");
my $writer = new XML::Writer(OUTPUT => $doc);
$writer->xmlDecl('UTF-8');

  $writer->startTag('wpmpaymentrequest', 'msgid' => $msgid);
    $writer->dataElement( 'clientid', $clientid);
    $writer->dataElement( 'requesttype', "1");
    $writer->dataElement( 'pathwayid', $pathwayid);
    $writer->dataElement( 'departmentid', $deptid);
    $writer->dataElement( 'staffid', $staffid);
    $writer->dataElement( 'customerid', "<![CDATA[".$customerid."]]>");
    $writer->dataElement( 'title', "<![CDATA[".$title."]]>");
    $writer->dataElement( 'firstname', "<![CDATA[".$firstname."]]>");
    $writer->dataElement( 'lastname', "<![CDATA[".$lastname."]]>");
    $writer->dataElement( 'emailfrom', "<![CDATA[library\@strath.ac.uk]]>");
    $writer->dataElement( 'toemail', "<![CDATA[".$email."]]>");
    $writer->dataElement( 'transactionreference', "<![CDATA[".$transactionref."]]>");
    $writer->dataElement( 'redirecturl', "<![CDATA[".$redirect."]]>");
    $writer->dataElement( 'callbackurl', "<![CDATA[".$callback."]]>");
    $writer->dataElement( 'cancelurl', "<![CDATA[".$cancel."]]>");
    $writer->dataElement( 'customfield1', "<![CDATA[".."]]>");
    $writer->dataElement( 'customfield2', "<![CDATA[".."]]>");
    $writer->dataElement( 'customfield3', "<![CDATA[".."]]>");
    $writer->dataElement( 'customfield4', "<![CDATA[".."]]>");
    $writer->dataElement( 'customfield5', "<![CDATA[".."]]>");
    $writer->dataElement( 'customfield6', "<![CDATA[".."]]>");
    $writer->dataElement( 'customfield7', "<![CDATA[".."]]>");
    $writer->dataElement( 'customfield8', "<![CDATA[".."]]>");
    $writer->dataElement( 'customfield9', "<![CDATA[".."]]>");
    $writer->dataElement( 'customfield10', "<![CDATA[".."]]>");

    $writer->startTag('payments', 'id' => "1", 'type' => "PN", 'payoption' => "Library Fine");
    	$writer->dataElement( 'description', "<![CDATA[".."]]>");
foreach (my $i=1; $i<=$fcount; $i++){
my $itemid = $payid-1;
my $fnType= $valu[0][$itemid]{'type'};
my $fnDate= $valu[0][$itemid]{'date'};
my $fnBalance= $valu[0][$itemid]{'balance'};
my $fnBarcode= $valu[0][$itemid]{'barcode'};

 		$writer->startTag('payment', 'payid' => $payid);
			$writer->dataElement( 'customfield1', "<![CDATA[".$fnType."]]>");
			$writer->dataElement( 'customfield2', "<![CDATA[".$fnDate."]]>");
			$writer->dataElement( 'customfield3', "<![CDATA[".$fnBalance."]]>");
			$writer->dataElement( 'customfield4', "<![CDATA[".$fnBarcode."]]>");
			$writer->dataElement( 'customfield5', "<![CDATA[".."]]>");
			$writer->dataElement( 'customfield6', "<![CDATA[".."]]>");
			$writer->dataElement( 'customfield7', "<![CDATA[".."]]>");
			$writer->dataElement( 'customfield8', "<![CDATA[".."]]>");
			$writer->dataElement( 'customfield9', "<![CDATA[".."]]>");
			$writer->dataElement( 'customfield10', "<![CDATA[".."]]>");
	   		$writer->dataElement( 'amounttopay', $fnBalance );
			$writer->dataElement( 'amounttopayvat');
			$writer->dataElement( 'amounttopayexvat',$fnBalance);
			$writer->dataElement( 'vatdesc');
			$writer->dataElement( 'vatcode');
			$writer->dataElement( 'vatrate',"0");
			$writer->dataElement( 'dateofpayment', $rightnow);
			$writer->startTag( 'editable', 'minamount' => "0", 'maxamount' => "0", );
				$writer->characters("0");
			$writer->endTag();   
			$writer->dataElement( 'mandatory', "0");
		$writer->endTag();                        # close section   
$payid++;
}	$writer->endTag();                        # close section 

	$writer->dataElement( 'backurl');
$writer->startTag('transaction');
		$writer->dataElement( 'transid', "CPG67569" );
		$writer->dataElement( 'authcode', "080580" );
		$writer->dataElement( 'preauth', "0" );
		$writer->dataElement( 'dateofpayment', "2013-11-29 09:57:45" );
		$writer->dataElement( 'totalpaid', "10.00" );
		$writer->dataElement( 'success', "1" );
		$writer->dataElement( 'failurereason' );
		$writer->startTag('carddetails');
			$writer->dataElement( 'cardholdername', "Mr Butler" );	
			$writer->dataElement( 'cardtype', "vsd" );			
			$writer->dataElement( 'cardlast4', "8116" );			
			$writer->dataElement( 'cardexpdate',"062015" );			
			$writer->dataElement( 'cardstartdate', "062012" );			
			$writer->dataElement( 'cardissuenumber' );			
		$writer->endTag();                        # close section   
	$writer->endTag();                        # close section   
$writer->endTag(); 

 $writer->end();                           
 $doc->close(); 

#read XML into a string variable
my $sendXml = "payment_xml/$msgid-test.xml";
my $WPM = "http://pumblechook.lib.strath.ac.uk:7004/cgi-bin/paymentUpdate.cgi";
my $resultXML;

open(XML, $sendXml);
while(<XML>){
$message .="$_";
}
#post the generated XML string to WPM
close XML;
my $ua = LWP::UserAgent->new;
 $ua->timeout(10);
 $ua->env_proxy;
 
 my $response = $ua->post($WPM, Content_Type => 'text/xml', Content => $message); 

 if ($response->is_success) {
     print $response->decoded_content;  # or whatever

 }
 else {
     die $response->status_line;

 }
